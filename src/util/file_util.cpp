#include "util/file_util.h"

void write_content_to_file(const char * file_path, const char * file_name,
		const char * content)
{
	std::time_t t = std::time(0);
	std::tm * lt = std::localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(lt, "_%Y-%m-%d %H.%M.%S");
	std::string date = oss.str();

	std::ofstream signature_file(std::string(file_path) + "/" + std::string(file_name) + date + ".p7s");

	signature_file << content;

	signature_file.close();

}
