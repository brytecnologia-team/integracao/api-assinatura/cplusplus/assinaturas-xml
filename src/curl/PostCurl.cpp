
#include "curl/PostCurl.h"

PostCurl::PostCurl(bool verbose) : Curl()
{	
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_response_body);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*) &response);
    if (verbose)
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    else
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);
    
    form = curl_mime_init(curl);
}

PostCurl::~PostCurl()
{
    curl_mime_free(form);
    delete response;
}

void PostCurl::perform() 
{
    if (list) {
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    }
    if (post.size() > 0) {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post.c_str());
    }
    if (field) {
        curl_easy_setopt(curl, CURLOPT_MIMEPOST, form);
    }

    Curl::perform();
}

void PostCurl::addHeader(String key, String value) 
{
    String valor =  key + ":" + value;
    list = curl_slist_append(list, valor.c_str());
}

void PostCurl::addBody(String key, String value, bool urlFormat) 
{
    String valor;
    if (urlFormat)
        valor = Curl::urlFormat(key) + "=" + Curl::urlFormat(value);
    else
        valor = key + "=" + value;

    if (post.size() > 0)
        valor = "&" + valor;

    post = post + valor;
}

void PostCurl::setJson(String json) 
{
    post = json;
}

json_object * PostCurl::getJsonResponse() 
{
    json_tokener* tok = json_tokener_new();
    json_object * response_as_json = json_tokener_parse_ex(tok, response -> c_str(), response -> size());
    if (!response_as_json) {
        enum json_tokener_error error = json_tokener_get_error(tok);
        String message = json_tokener_error_desc(error);
        message = "Error no parser json:" + message + "\nResponse:" + response -> c_str();
        throw new JsonFormatException(message.c_str());
    }
    
    return response_as_json;
}

void PostCurl::addMime(String key, String value)
{
    field = curl_mime_addpart(form);
    curl_mime_name(field, key.c_str());
    curl_mime_data(field, value.c_str(), CURL_ZERO_TERMINATED);
}

void PostCurl::addFile(String key, String filename)
{
    field = curl_mime_addpart(form);
    curl_mime_name(field, key.c_str());
    curl_mime_filedata(field, filename.c_str());
}