#include "crypto/PKCS1DTO.h"

PKCS1DTO::PKCS1DTO()
: _nonce(0), _content(0), _signature_value(0), _file_content(0) {};

PKCS1DTO::PKCS1DTO(const char * nonce, const char * content)
: _nonce(nonce), _content(content), _signature_value(0), _file_content(0) {};

PKCS1DTO::~PKCS1DTO()
{
	delete[] _file_content;
	delete[] _nonce;
	delete[] _content;
	delete[] _signature_value;
}

const char * PKCS1DTO::get_nonce() { return _nonce; };
void PKCS1DTO::set_nonce(const char * nonce) { _nonce = nonce; };

const char * PKCS1DTO::get_content() { return _content; };
void PKCS1DTO::set_content(const char * content) { _content = content; };

const char * PKCS1DTO::get_file_content() { return _file_content; };
void PKCS1DTO::set_file_content(const char * file_content) { _file_content = file_content; };

unsigned char * PKCS1DTO::get_signature_value() { return _signature_value; };
void PKCS1DTO::set_signature_value(unsigned char * signature_value) { _signature_value = signature_value; };

const char * PKCS1DTO::get_file() { return _file; }
void PKCS1DTO::set_file(const char * file) { _file = file; };
