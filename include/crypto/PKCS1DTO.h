#ifndef __PKCS1DTO_h
#define __PKCS1DTO_h

#include <string>

class PKCS1DTO
{
public:
	PKCS1DTO();
	PKCS1DTO(const char * nonce, const char * content);
	~PKCS1DTO();

	const char * get_nonce();
	void set_nonce(const char * nonce);

	const char * get_content();
	void set_content(const char * content);

	const char * get_file_content();
	void set_file_content(const char * content);

	unsigned char * get_signature_value();
	void set_signature_value(unsigned char * signature_value);

	std::string get_signature_string() { return signature_string; }
	void set_signature_string(std::string str) { signature_string = str; }

	const char * get_file();
	void set_file(const char * file);

private:
	const char * _nonce;
	const char * _content;
	unsigned char * _signature_value;
	const char * _file_content;
	const char * _file;

	std::string signature_string;
};
#endif
