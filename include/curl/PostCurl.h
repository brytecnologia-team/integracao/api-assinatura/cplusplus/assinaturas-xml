#ifndef __POST_SERVICE_H
#define __POST_SERVICE_H

#include <stdexcept>
#include <json-c/json.h>

#include "curl/Curl.h"
#include "util/http_util.h"
#include "exception/JsonFormatException.h"

class PostCurl : public Curl {
typedef std::string String;

private:
    struct curl_slist *list = NULL;
    curl_mime *form = NULL;
    curl_mimepart *field = NULL;
	String * response = new String("");
    String post = "";

    String urlFormat(String value);

public:
    PostCurl(bool verbose = false);
    ~PostCurl();
    void perform();
    void setJson(String json);
    void addHeader(String key, String value);
    void addBody(String key, String value, bool urlFormat = true);
    void addMime(String key, String value);
    void addFile(String key, String filename);
    json_object * getJsonResponse();

    String * getResponse(){ return response; }
    String getPost() { return post; }
};

#endif