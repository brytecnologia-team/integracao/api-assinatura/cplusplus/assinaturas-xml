#ifndef __get_url_from_json_h
#define __get_url_from_json_h

#include <string>
#include <json-c/json.h>

#include "exception/JsonFormatException.h"

std::string get_url_from_json(json_object * object);

#endif
