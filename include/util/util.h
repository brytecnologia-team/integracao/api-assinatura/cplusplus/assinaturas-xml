#ifndef __util_h
#define __util_h

#include <iostream>
#include <string.h>
#include <algorithm>
#include <openssl/evp.h>

#define BEGIN_CERTIFICATE "-----BEGIN CERTIFICATE-----"
#define END_CERTIFICATE "-----END CERTIFICATE-----"

const char * remove_substring(const char * text, const char * to_remove);

unsigned char * cert_pem_to_base64(const char * cert_pem);



#endif
