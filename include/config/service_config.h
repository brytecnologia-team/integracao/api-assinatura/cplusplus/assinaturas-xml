#ifndef __service_config_h
#define __service_config_H

#define URL_SERVER "https://hub2.hom.bry.com.br"

#define URL_SERVER_XML URL_SERVER "/api/xml-signature-service/v2/signatures/"

#define URL_INITIALIZE_SIGNATURE URL_SERVER_XML "initialize"

#define URL_FINALIZE_SIGNATURE URL_SERVER_XML "finalize"

#define ACCESS_TOKEN "<ACESS_TOKEN>"

#endif
