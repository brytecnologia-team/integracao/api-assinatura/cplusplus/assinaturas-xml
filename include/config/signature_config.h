#ifndef __signature_config_h
#define __signature_config_h

#include <string>

#define ATTACHED "true"

#define OPERATION_TYPE "SIGNATURE"

#define RETURN_TYPE_INIT "BASE64"

#define RETURN_TYPE_FIN "LINK"

#define SIGNATURE_FORMAT "ENVELOPED"

#define HASH_ALGORITHM "SHA256"

#define PROFILE "ADRC"

#define NUMBER_OF_DOCUMENTS 1

const std::string ORIGINAL_DOCUMENTS_LOCATION[] = {"./resources/documents/sample.xml"};

#define OUTPUT_RESOURCE_FOLDER "./resources/generatedSignatures/"

#endif
